% Dummy data for generating a plot

rate1 = [.6064 .6551 .6683 .6990 .7141 .7185]*100;
rate2 = [.6449 .6961 .7122 .7361 .7346 .7405]*100; 
plot(rate2,'b');
hold on;
plot(rate1,'r--');
legend({'27-part model','14-part model'});

% Main trick:
% (1) interactively resize figure window so that it is as small as
% possible, while retaining crucial info. Matlab will automatically
% resize text and axis ticks to be readable
% (2) store the (interactively-discovered) position in a variable,
% so that figure can be later regenerated without interaction
% (3) call function 'savepdf', which will save figure as sized on screen
%
% tposition = get(gcf,'position');
tposition = [2029 522 338 307];
set(gcf,'position',tposition);
set(gca,'ytick',60:2:75);
grid on;
title('Performance vs number of types per part');
savepdf('structureplot');
